package com.luanlcs.excecoes;

public class SenhaInvalidaException extends Exception {
    public SenhaInvalidaException(String message) {
        super(message);
    }
}
