package com.luanlcs.excecoes;

public class SenhaTest {
    private static void autenticar(String senha) throws SenhaInvalidaException {
        if (!"123".equals(senha)) {
            throw new SenhaInvalidaException("Senha inválida.");
        }
        System.out.println("Autenticado!");
    }
    public static void main(String[] args) {

        try {
            autenticar("484");
        } catch (SenhaInvalidaException e) {
            System.err.println(e.getMessage());
        }

        try {
            autenticar("123");
        } catch (SenhaInvalidaException e) {
            System.err.println(e.getMessage());
        }


    }
}
