package com.luanlcs.excecoes;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExcecaoTest1 {
    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);
        boolean continua = true;

        do {
            try {

                System.out.print("Número: ");
                int numero = entrada.nextInt();

                System.out.print("Divisor: ");
                int divisor = entrada.nextInt();

                int resultado = numero / divisor;
                System.out.println("Resultado: " + resultado);

                continua = false;
            } catch (InputMismatchException ex) {
                System.err.println("Digite valores inteiros válidos.");
                entrada.nextLine();
            } catch (ArithmeticException ex) {
                System.err.println("O divisor deve ser diferente de zero.");
            } finally {
                System.out.println("Finally executado.");
            }
        } while (continua);

        entrada.close();

    }
}
