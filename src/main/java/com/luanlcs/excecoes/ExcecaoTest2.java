package com.luanlcs.excecoes;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExcecaoTest2 {
    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);
        boolean continua = true;

        do {
            try {
                dividir(entrada);
                continua = false;
            } catch (InputMismatchException | ArithmeticException ex) {
                System.err.println("Digite valores inteiros. Obs: O dividor deve ser diferente de zero.");
                // ex.printStackTrace();
                System.err.println(ex.getMessage());
                entrada.nextLine();
            } finally {
                System.out.println("Finally executado.");
            }
        } while (continua);

        entrada.close();

    }

    private static void dividir(Scanner entrada) throws ArithmeticException, InputMismatchException {
        System.out.print("Número: ");
        int numero = entrada.nextInt();

        System.out.print("Divisor: ");
        int divisor = entrada.nextInt();

        int resultado = numero / divisor;
        System.out.println("Resultado: " + resultado);
    }
}
