package com.luanlcs.fundamentos.estruturas.repeticao;

@SuppressWarnings("all")
public class RepeticaoBreakContinue {
    public static void main(String[] args) {

        // break - interrompe o laço
        for (int i = 0; i <= 10; i++) {
            if (i == 5) {
                break;
            }
            System.out.print(i + " ");
        }
        System.out.println();

        // continue - interrompe a iteração atual
        for (int i = 0; i <= 10; i++) {
            if (i == 5) {
                continue;
            }
            System.out.print(i + " ");
        }
        System.out.println();

        // rotulos
        boolean[][] matriz = {{false, false, true, false, false}, {false, false, false, false, false}};

        externo:
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (matriz[i][j]) {
                    break externo;
                }
                System.out.println(matriz[i][j]);
            }
        }

    }
}
