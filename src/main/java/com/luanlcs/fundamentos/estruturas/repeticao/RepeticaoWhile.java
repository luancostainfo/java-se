package com.luanlcs.fundamentos.estruturas.repeticao;

@SuppressWarnings("all")
public class RepeticaoWhile {
    public static void main(String[] args) {

        int contador = 0;

        while (contador <= 10) {
            System.out.print(contador + " ");
            contador++;
        }
        System.out.println();

        do {
            System.out.println("Executa pelo menos uma vez");
        } while (false);
    }
}
