package com.luanlcs.fundamentos.estruturas.repeticao;

import java.util.ArrayList;

public class RepeticaoForEach {
    public static void main(String[] args) {

        ArrayList<Integer> numeros = new ArrayList<>();
        for (int i = 0; i <= 20; i++) {
            numeros.add(i);
        }

        // percorrendo com enhanced-for
        for (Integer numero : numeros) {
            System.out.print(numero + " ");
        }
        System.out.println();

        // percorrendo sem enhanced-for
        for (int i = 0; i < numeros.size(); i++) {
            System.out.print(i + " ");
        }
    }
}
