package com.luanlcs.fundamentos.estruturas.repeticao;

public class RepeticaoExercicio {
    public static void main(String[] args) {

        int anterior = 0;
        int proximo = 1;

        System.out.println(anterior);
        System.out.println(proximo);

        while (proximo < 50) {
            proximo = proximo + anterior;
            anterior = proximo - anterior;
            System.out.print(proximo + " ");
        }
    }
}
