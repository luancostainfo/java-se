package com.luanlcs.fundamentos.estruturas.condicional;

@SuppressWarnings("all")
public class CondicionalIfElse {
    public static void main(String[] args) {

        // exemplo #01
        int idade = 10;
        if (idade < 11) {
            System.out.println("Criança");
        }

        // exemplo #02
        boolean passou = true;
        if (passou) {
            System.out.println("Contratado");
        }

        // exemplo #03
        int numero = 10;
        if (numero % 2 == 0) {
            System.out.println("Par");
        } else {
            System.out.println("Ímpar");
        }

        // exemplo #04
        idade = 27;
        if (idade <= 11) {
            System.out.println("Criança");
        } else if (idade > 11 && idade <= 18) {
            System.out.println("Adolescente");
        } else if (idade > 18 && idade <= 60) {
            System.out.println("Adulto");
        } else {
            System.out.println("Melhor idade");
        }

        // exemplo #05
        int nota = 6;
        if (nota >= 7) {
            System.out.println("Aprovado");
        } else {
            System.out.print("Reprovou");
            if (nota >= 6) {
                System.out.println(", mas pode fazer a recuperação.");
            }
        }


    }
}
