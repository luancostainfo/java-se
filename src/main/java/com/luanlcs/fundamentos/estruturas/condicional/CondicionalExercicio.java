package com.luanlcs.fundamentos.estruturas.condicional;

import javax.swing.*;
import java.util.Random;

public class CondicionalExercicio {
    public static void main(String[] args) {

        int palpite = Integer.parseInt(JOptionPane.showInputDialog("Digite um número de 1 a 6"));
        int numeroSorteado = new Random().nextInt(6) + 1;
        String resultado = numeroSorteado == palpite ? "Acertou :D" : "Errou :(";

        String mensagem = String.format("%s%nSeu palpite: %s%nNúmero sorteado: %d", resultado, palpite, numeroSorteado);
        JOptionPane.showMessageDialog(null, mensagem);

    }
}
