package com.luanlcs.fundamentos.estruturas.condicional;

@SuppressWarnings("all")
public class CondicionalSwitch {
    public static void main(String[] args) {

        // exemplo #01
        char sexo = 'M';
        switch (sexo) {
            case 'M':
                System.out.println("Masculino");
                break;
            case 'F':
                System.out.println("Feminino");
                break;
            default:
                System.out.println("Inválido");
                break;
        }

        // exemplo #02
        String tecnologia = "Java";
        switch (tecnologia) {
            case "Java":
            case "C#":
            case "PHP":
                System.out.println("Linguagem de programação");
                break;
            case "MySQL":
            case "DB2":
            case "Oracle DB":
                System.out.println("SGBD");
                break;
            default:
                System.out.println("Tecnologia desconhecida");
                break;
        }

    }
}
