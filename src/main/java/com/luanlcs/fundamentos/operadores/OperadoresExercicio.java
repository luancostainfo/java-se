package com.luanlcs.fundamentos.operadores;

import javax.swing.*;

public class OperadoresExercicio {
    public static void main(String[] args) {
        double pesoDeQuilogramas = Double.parseDouble(JOptionPane.showInputDialog("Peso de quilogramas"));
        double alturaEmMetros = Double.parseDouble(JOptionPane.showInputDialog("Altura em metros"));
        double imc = pesoDeQuilogramas / alturaEmMetros;

        String mensagem = "Seu IMC é " + Math.round(imc);
        JOptionPane.showMessageDialog(null, mensagem);
    }
}
