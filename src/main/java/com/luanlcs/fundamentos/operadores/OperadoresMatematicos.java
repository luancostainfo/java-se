package com.luanlcs.fundamentos.operadores;

public class OperadoresMatematicos {
    public static void main(String[] args) {

        // operadores

        int soma = 10 + 5;
        System.out.println(soma);

        int subtracao = 10 - 5;
        System.out.println(subtracao);

        int divisao = 10 / 5;
        System.out.println(divisao);

        int multiplicacao = 10 * 5;
        System.out.println(multiplicacao);

        int modulo = 10 % 5;
        System.out.println(modulo);

        // unarios - e +
        int a = +10; // positivo explicito
        int b = -(-10); // inverte o sinal

        System.out.println(a);
        System.out.println(b);

        // decremento e incremento
        int x = 50;
        int y = x++; // atribui o valor de x para y e então incrementa x

        System.out.println(x);
        System.out.println(y);

        x = 100;
        y = ++x; // incrementa o valor x e então atribui o valor de x para y
        System.out.println(x);
        System.out.println(y);

        // concatenacao de strings
        String texto = "Estudo " + "de " + "operadores";
        System.out.println(texto);
    }
}
