package com.luanlcs.fundamentos.operadores;

@SuppressWarnings("all")
public class OperadoresRelacionais {
    public static void main(String[] args) {

        int x = 50;

        System.out.println(x == 50);
        System.out.println(x != 51);

        System.out.println(x > 49);
        System.out.println(x >= 50);

        System.out.println(x < 49);
        System.out.println(x <= 49);

        Integer y = 100;
        System.out.println(y instanceof Integer);
        System.out.println("Luan" instanceof String);
    }
}
