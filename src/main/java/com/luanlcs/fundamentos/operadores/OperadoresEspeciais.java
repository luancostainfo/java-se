package com.luanlcs.fundamentos.operadores;

@SuppressWarnings("all")
public class OperadoresEspeciais {
    public static void main(String[] args) {

        // operador ternario
        int idade = 6;

        // expressao ? true : false

        String resultado = idade >= 18 ? "Maior de idade" : "Menor de idade";
        System.out.println(resultado);

        // Separador de expressões
        String primeiroNome, segundoNome;
        primeiroNome = "Luan";
        segundoNome = "da Costa";
        System.out.println(primeiroNome + " " + segundoNome);

        // praticando

        // diametro: 2r

        double raio = 10;
        double diametro = 2 * raio;
        System.out.println(diametro);

        // circunferencia: 2 * PI * raio
        double circunferencia = 2 * Math.PI * raio;

        // area: PI * raio²
        double area = Math.PI * raio * raio;
        System.out.println(area);
    }
}
