package com.luanlcs.fundamentos.operadores;

@SuppressWarnings("all")
public class OperadoresLogicos {
    public static void main(String[] args) {

        System.out.println(true && true);
        System.out.println(true && false);
        System.out.println(false && false);

        System.out.println();

        System.out.println(true || true);
        System.out.println(true || false);
        System.out.println(false || false);

        System.out.println();

        System.out.println(true ^ true);
        System.out.println(true ^ false);
        System.out.println(false ^ false);

        System.out.println();

        System.out.println(!true);
        System.out.println(!false);
    }
}
