package com.luanlcs.fundamentos.operadores;

public class OperadoresDeAtribuicao {
    public static void main(String[] args) {

        int a = 10;
        System.out.println(a);

        a += 5; // a = a + 5
        System.out.println(a);

        a -= 5; // a = a - 5
        System.out.println(a);

        a *= 5; // a = a * 5
        System.out.println(a);

        a /= 5; // a = a / 5
        System.out.println(a);

        a %= 5; // a = a % 5
        System.out.println(a);

    }
}
