package com.luanlcs.fundamentos.basico;

import javax.swing.*;

public class EntradaJOptionPane {
    public static void main(String[] args) {

        String nome = JOptionPane.showInputDialog("Digite seu nome");
        int idade = Integer.parseInt(JOptionPane.showInputDialog("Digite sua idade"));
        double salario = Double.parseDouble(JOptionPane.showInputDialog("Digite seu salario"));

        String mensagem = String.format("%s tem %d anos de idade e recebe %.2f", nome, idade, salario);
        JOptionPane.showMessageDialog(null, mensagem);

    }
}
