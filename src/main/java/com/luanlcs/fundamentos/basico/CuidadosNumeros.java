package com.luanlcs.fundamentos.basico;

import java.math.BigDecimal;

@SuppressWarnings("all")
public class CuidadosNumeros {
    public static void main(String[] args) {

        // alguns cuidados com valores numericos

        // numeros que iniciam com 0 estão na base octal

        int octal = 0723;
        System.out.println(octal); // 467

        // divisao entre inteiros sempre retorna um inteiro
        int divisao = 5 / 2; // 2
        System.out.println(divisao);

        double outraDivisao = 5 / 2;
        System.out.println(outraDivisao);

        // operacoes com flutuantes
        double valor1 = 0.1;
        double valor2 = 0.2;
        double total = valor1 + valor2;

        System.out.println(total);

        BigDecimal value1 = new BigDecimal("0.1");
        BigDecimal value2 = new BigDecimal("0.2");

        BigDecimal sum = value1.add(value2);
        System.out.println(sum);

    }
}
