package com.luanlcs.fundamentos.basico;

public class IntroducaoGarbageCollector {
    public static void main(String[] args) {

        int a = 10; // variavel 'a' de fato armazena o valor 10
        System.out.println(a);

        // a variavel nome armazena o endereço de memória do objeto 'Luan'.
        String nome = "Luan";
        System.out.println(nome);


        /*
         * 'nome' deixa de apontar para 'Luan' e passa a apontar para 'Alen'. O objeto 'Luan' ficou sem nenhuma
         * referência, logo será coletado pelo Garbage Collector
         * */
        nome = "Alen";
        System.out.println(nome);


        /*
         * Agora 'nome' não aponta para nenhum objeto, logo 'Alen' também será coletado pelo GarbageCollector
         * */
        nome = null;
    }
}
