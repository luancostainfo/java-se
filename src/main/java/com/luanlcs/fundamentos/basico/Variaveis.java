package com.luanlcs.fundamentos.basico;

public class Variaveis {
    public static void main(String[] args) {
        String nome = "Luan";
        boolean casado = false;
        double salario = 9200;
        int idade = 27;

        System.out.println("Nome: " + nome);
        System.out.println("Casado: " + casado);
        System.out.println("Salário: " + salario);
        System.out.println("Idade: " + idade);
    }
}
