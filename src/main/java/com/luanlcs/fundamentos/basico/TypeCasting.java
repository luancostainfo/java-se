package com.luanlcs.fundamentos.basico;

public class TypeCasting {
    public static void main(String[] args) {

        // cast implicito - ocorre automaticamente

        int a = 10;
        double b = a;

        System.out.println(a);
        System.out.println(b);

        // cast explicito - temos que informar o tipo que queremos realizar a conversao

        // sem perdas
        long c = 10;
        int d = (int) c;

        System.out.println(c);
        System.out.println(d);

        // com perdas
        double e = 10.59;
        int f = (int) e;

        System.out.println(e);
        System.out.println(f);
    }
}
