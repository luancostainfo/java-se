package com.luanlcs.fundamentos.basico;

public class Constantes {
    public static void main(String[] args) {

        // variaveis podem sofrer alterações
        int idade = 23;
        System.out.println(idade);

        idade = 26;
        System.out.println(idade);

        // constantes armazenam valores que nunca mudam

        final char SEXO_MASCULINO = 'M';
        final char SEXO_FEMININO = 'F';

        final double PI = 3.14;

        System.out.println(SEXO_MASCULINO);
        System.out.println(SEXO_FEMININO);

    }
}
