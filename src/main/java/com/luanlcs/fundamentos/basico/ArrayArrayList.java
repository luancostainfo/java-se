package com.luanlcs.fundamentos.basico;

import java.util.ArrayList;

public class ArrayArrayList {
    public static void main(String[] args) {

        ArrayList<String> cores = new ArrayList<>();
        cores.add("Red");
        cores.add("Green");
        cores.add("Blue");
        cores.add("Black");
        cores.add("White");

        // recuperando elementos
        System.out.println(cores.get(0));
        System.out.println(cores.get(3));

        // quantidade de elementos
        System.out.println(cores.size());

        // removendo elementos

        boolean removeu = cores.remove("Blue");
        System.out.println(removeu);

        String elementoRemovido = cores.remove(2);
        System.out.println(elementoRemovido);
        System.out.println(cores);

        // recuperando indice de determinado elemento
        int indiceDeGreen = cores.indexOf("Green");
        System.out.println(indiceDeGreen);

        // verificando se determinado elemento está na lista
        boolean temWhite = cores.contains("White");
        System.out.println(temWhite);

        boolean temBlack = cores.contains("Black");
        System.out.println(temBlack);
    }
}
