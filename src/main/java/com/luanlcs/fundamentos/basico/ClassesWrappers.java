package com.luanlcs.fundamentos.basico;

@SuppressWarnings("all")
public class ClassesWrappers {
    public static void main(String[] args) {

        // Constantes
        System.out.println(Integer.TYPE);
        System.out.println(Integer.BYTES);
        System.out.println(Integer.SIZE);
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);

        // de string para inteiro primitivo
        int a = Integer.parseInt("10");
        System.out.println(a);

        int b = Integer.parseInt("010101011", 2);
        System.out.println(b);

        // de string para inteiro wrapper
        Integer c = Integer.valueOf("20");
        System.out.println(c);

        Integer d = Integer.valueOf("0101010", 2);
        System.out.println(d);

        // metodos auxiliares
        Integer e = 59;
        System.out.println(e.intValue());
        System.out.println(e.doubleValue());

        //
        System.out.println(Integer.compare(0, 0));
        System.out.println(Integer.compare(1, 0));
        System.out.println(Integer.compare(0, 1));

        System.out.println(Integer.max(10, 5));
        System.out.println(Integer.min(10, 5));

        System.out.println(Integer.MAX_VALUE);

        System.out.println(Integer.toBinaryString(17111995));
        System.out.println(Integer.toHexString(17111995));
        System.out.println(Integer.toOctalString(17111995));

        System.out.println(Integer.toString(1000));
    }
}
