package com.luanlcs.fundamentos.basico;

public class TiposPrimitivos {
    public static void main(String[] args) {

        // inteiros
        byte varByte = 127;
        System.out.println(varByte);

        short varShort = 32_767;
        System.out.println(varShort);

        int varInt = 2_147_483_647;
        System.out.println(varInt);

        long varLong = 9_223_372_036_854_775_807L;
        System.out.println(varLong);

        // flutuantes - IEEE 754
        float varFloat = 3.4028235E38F;
        System.out.println(varFloat);

        double varDouble = 1.7976931348623157E308;
        System.out.println(varDouble);

        // boolean
        boolean verdade = true;
        boolean mentira = false;

        System.out.println(verdade);
        System.out.println(mentira);

        // texto
        char letra = 'A';
        char outraLetra = 66;

        System.out.println(letra);
        System.out.println(outraLetra);
    }
}
