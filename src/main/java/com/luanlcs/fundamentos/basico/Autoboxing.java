package com.luanlcs.fundamentos.basico;

@SuppressWarnings("all")
public class Autoboxing {
    public static void main(String[] args) {

        // antes do Java 5
        Integer x = new Integer(10); // empacotado
        int y = x.intValue(); // desempacotado
        y++; // incrementa
        x = new Integer(y); // re-empacota
        System.out.println(x);

        Boolean aTrue = new Boolean("true");
        if (aTrue) {
            System.out.println(aTrue);
        }

        // a partir do Java 5
        Integer z = 50;
        z++;
        System.out.println(z);


    }
}
