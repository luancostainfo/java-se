package com.luanlcs.fundamentos.basico;

import java.util.Scanner;

public class EntradaScanner {
    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);

        System.out.print("Nome: ");
        String nome = entrada.nextLine();

        System.out.print("Idade: ");
        int idade = entrada.nextInt();

        System.out.print("Salário: ");
        double salario = entrada.nextDouble();

        String mensagem = String.format("%s tem %d anos de idade e recebe %.2f", nome, idade, salario);
        System.out.println(mensagem);

        entrada.close();

    }
}
