package com.luanlcs.fundamentos.basico;

import java.util.Arrays;

public class ArraySimples {
    public static void main(String[] args) {

        // criacao
        int[] numeros = new int[5];
        numeros[0] = 2;
        numeros[1] = 4;
        numeros[2] = 6;
        numeros[3] = 8;
        numeros[4] = 10;

        System.out.println(Arrays.toString(numeros));

        // notacao literal
        String[] paises = {"Brasil", "Russia", "India", "China", "Africa do Sul"};

        String primeiroPais = paises[0];
        System.out.println(primeiroPais);

        paises[0] = "BRAZIL";
        System.out.println(paises[0]);

        // classe Arrays
        System.out.println(Arrays.toString(paises));

        // pesquisa
        int indiceRussia = Arrays.binarySearch(paises, "Russia");
        System.out.println(indiceRussia);

        // ordenacao
        Arrays.sort(paises);
        System.out.println(Arrays.toString(paises));
    }
}
