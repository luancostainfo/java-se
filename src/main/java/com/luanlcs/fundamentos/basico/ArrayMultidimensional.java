package com.luanlcs.fundamentos.basico;

import java.util.Arrays;

public class ArrayMultidimensional {
    public static void main(String[] args) {

        String[] uma = {"Luan", "Larissa", "Juliana"};
        System.out.println(Arrays.toString(uma));

        String[][] duas = {{"Luan", "M", "DF"}, {"Larrisa", "F", "DF"}};
        String nomePrimeiroElemento = duas[0][0];
        String nomeSegundoElemento = duas[1][0];

        System.out.println(nomePrimeiroElemento);
        System.out.println(nomeSegundoElemento);

        System.out.println(Arrays.toString(duas[0]));
        System.out.println(Arrays.toString(duas[1]));

    }
}
