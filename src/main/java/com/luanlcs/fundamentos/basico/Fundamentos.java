package com.luanlcs.fundamentos.basico;

/**
 * Classe utilizada para demonstrar fundamentos de Java
 *
 * @author Luan da Costa Silva
 */
public class Fundamentos {
    public static void main(String[] args) {
        // comentario de uma linha

        /*
         * comentario
         * de
         * varias linhas
         */

        String nome = "Luan";
        System.out.println(nome);

        // caracteres de escape
        System.out.println("Linha 1\nLinha2");
        System.out.println("Tab \t Tab");
        System.out.println("\"Aspas Duplas\"");
        System.out.println("Barra invertida \\");
    }
}
