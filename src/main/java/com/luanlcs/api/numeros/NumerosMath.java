package com.luanlcs.api.numeros;

public class NumerosMath {
    public static void main(String[] args) {

        System.out.println(Math.E);
        System.out.println(Math.PI);

        System.out.println(Math.abs(-10));
        System.out.println(Math.sin(10));
        System.out.println(Math.cos(10));
        System.out.println(Math.tan(10));
        System.out.println(Math.hypot(10, 2));
        System.out.println(Math.sqrt(9));
        System.out.println(Math.cbrt(9));
        System.out.println(Math.max(10, 20));
        System.out.println(Math.min(10, 5));
        System.out.println(Math.random()); // 0 - 9
        System.out.println(Math.ceil(5.5));
        System.out.println(Math.floor(5.5));
        System.out.println(Math.round(5.5));
        // ...
    }
}
