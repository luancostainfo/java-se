package com.luanlcs.api.numeros;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class NumeroInternacionalizacaoFormatacao {
    public static void main(String[] args) {

        double saldo = 123_456.789;

        NumberFormat formatter = NumberFormat.getInstance();
        System.out.println(formatter.format(saldo));

        formatter = NumberFormat.getIntegerInstance();
        System.out.println(formatter.format(saldo));

        formatter = NumberFormat.getPercentInstance();
        System.out.println(formatter.format(0.25));

        formatter = NumberFormat.getCurrencyInstance();
        formatter.setMaximumFractionDigits(5);
        System.out.println(formatter.format(saldo));

        // ---------- Internacionalização
        formatter = NumberFormat.getCurrencyInstance(Locale.US);
        System.out.println(formatter.format(saldo));

        formatter = NumberFormat.getCurrencyInstance(Locale.FRANCE);
        System.out.println(formatter.format(saldo));

        formatter = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        System.out.println(formatter.format(saldo));

        formatter = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        try {
            double preco = formatter.parse("R$ 123.456,79").doubleValue();
            System.out.println(preco);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
    }
}
