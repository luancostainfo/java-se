package com.luanlcs.api.colecoes;

import java.util.*;

public class ColecaoSet {
    public static void main(String[] args) {

        String[] cores = {"verde", "amarelo", "azul", "branco", "azul", "amarelo", "verde"};

        List<String> lista = Arrays.asList(cores);
        System.out.println(lista);

        HashSet<String> conjunto = new HashSet<>(lista);
        System.out.println(conjunto);

        Set<String> nomes = new LinkedHashSet<>(Arrays.asList("Luan", "Alen", "Igor", "Gabriel"));
        System.out.println(nomes);

        List<String> listaDeNomes = Arrays.asList("Luan", "Alen Kaleb", "Igor G.", "Gabriel Rocha");
        nomes = new TreeSet<>(listaDeNomes);
        System.out.println(nomes);

        nomes = new TreeSet<>(Comparator.comparing(String::length));
        nomes.addAll(listaDeNomes);
        System.out.println(nomes);

    }
}
