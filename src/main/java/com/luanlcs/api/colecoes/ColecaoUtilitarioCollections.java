package com.luanlcs.api.colecoes;

import java.util.*;

public class ColecaoUtilitarioCollections {
    public static void main(String[] args) {

        ArrayList<String> frutas = new ArrayList<>();
        frutas.add("Guaraná");
        frutas.add("Uva");
        frutas.add("Manga");
        frutas.add("Coco");
        frutas.add("Açaí");
        frutas.add("Banana");

        System.out.println(frutas);

        Collections.sort(frutas);
        System.out.println(frutas);

        Collections.reverse(frutas);
        System.out.println(frutas);

        Collections.shuffle(frutas);
        System.out.println(frutas);

        Collections.addAll(frutas, "Cupuaçu", "Laranja", "Laranja");
        System.out.println(frutas);

        int frequencia = Collections.frequency(frutas, "Laranja");
        System.out.println(frequencia);

        List<String> frutas2 = Arrays.asList("Acerola", "Graviola", "Laranja");

        /*
            verifica se as coleções possuem elementos em comum
            se tiverem retorna false
            se não tiverem retorna true
        */
        boolean disjoint = Collections.disjoint(frutas, frutas2);
        System.out.println(disjoint);

        Collections.sort(frutas);
        int indiceDeGuarana = Collections.binarySearch(frutas, "Guaraná");
        System.out.println(indiceDeGuarana);

        Collections.fill(frutas, "Açaí");
        System.out.println(frutas);

        // Coleção imutável
        Collection<String> frutasImutavel = Collections.unmodifiableCollection(frutas);
        System.out.println(frutasImutavel);

    }
}
