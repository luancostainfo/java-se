package com.luanlcs.api.colecoes;

import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("all")
public class ColecaoArrayListVsLinkedList {
    public static void main(String[] args) {

        List<Object> elementos = new LinkedList<>();
//        List<Object> elementos = new ArrayList<>();

        long inicio = System.currentTimeMillis();
        for (int i = 0; i < 100_000; i++) {
            elementos.add(0, i);
        }
        long fim = System.currentTimeMillis();
        long tempo = fim - inicio;

        System.out.println(tempo);

    }
}
