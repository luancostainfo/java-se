package com.luanlcs.api.colecoes;

import java.util.LinkedList;

public class ColecaoQueue {
    public static void main(String[] args) {

        LinkedList<String> fila = new LinkedList<>();
        fila.offer("Ricardo");
        fila.offer("Sandra");
        fila.offer("Beatriz");

        System.out.println(fila);

        String proximoASerAtendido = fila.peek();// proximo elemento da fila - nao remove
        System.out.println(proximoASerAtendido);

        System.out.println("Fila atual: " + fila);

        String clienteAtendido = fila.poll(); // remove o primeiro elemento
        System.out.println(clienteAtendido);

        System.out.println("Fila atual: " + fila);

        /* Outros métodos disponíveis em LinkedList */
        fila.addFirst("Caio");
        fila.addLast("Juliana");

        System.out.println("Fila atual: " + fila);

        proximoASerAtendido = fila.peekFirst();
        System.out.println("Próximo a ser atendido: " + proximoASerAtendido);

        String ultimoASerAtendido = fila.peekLast();
        System.out.println("Último a ser atendido: " + ultimoASerAtendido);

        System.out.println("Fila atual: " + fila);

        String primeiroRemovido = fila.pollFirst();
        String ultimoRemovido = fila.pollLast();

        System.out.println("Fila atual: " + fila);

        System.out.println("Primeiro removido: " + primeiroRemovido);
        System.out.println("Último removido: " + ultimoRemovido);

        System.out.println("Fila atual: " + fila);

    }
}
