package com.luanlcs.api.colecoes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class ColecaoCollection {
    public static void main(String[] args) {

        Collection<String> colecao = new ArrayList<>();

        colecao.add("A");
        colecao.add("E");
        colecao.add("I");

        System.out.println(colecao);
        System.out.println(colecao.isEmpty());
        System.out.println(colecao.contains("A"));

        boolean removeu = colecao.remove("A");
        System.out.println(removeu);

        System.out.println(colecao);

        /* Grupos */
        Collection<String> colecao2 = Arrays.asList("O", "U");
        colecao.addAll(colecao2);
        System.out.println(colecao);

        boolean contemTodos = colecao.containsAll(colecao2);
        System.out.println(contemTodos);

        boolean removeuTodos = colecao.removeAll(colecao2);
        System.out.println(removeuTodos);

        System.out.println(colecao);

        /* Percorrendo os elementos */
        for (String elemento : colecao) {
            System.out.println(elemento);
        }

        /* Convertendo Array para Lista */
        String[] letras = {"A", "B", "C"};
        List<String> lista = Arrays.asList(letras);
        System.out.println(lista);

        /* Convertendo Collection em Array */
        String[] array = lista.toArray(new String[0]);
        System.out.println(Arrays.toString(array));

        /* esvaziando Collection */
        colecao.clear();
        System.out.println(colecao);
    }
}
