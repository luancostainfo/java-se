package com.luanlcs.api.colecoes;

import java.util.ArrayList;

@SuppressWarnings("all")
public class ColecaoArrayList {
    public static void main(String[] args) {

        ArrayList<String> esportes = new ArrayList<>();
        esportes.add("Futebol");
        esportes.add("Basquete");
        esportes.add("Natação");
        esportes.add("Volei de Praia");
        esportes.add("Boxe");
        esportes.add("Futebol");

        System.out.println(esportes);

        for (int i = 0; i < esportes.size(); i++) {
            esportes.set(i, esportes.get(i).toUpperCase());
        }
        System.out.println(esportes);

        System.out.println(esportes.indexOf("BOXE"));
        System.out.println(esportes.subList(2, 4));

        esportes.subList(2, 4).clear();
        System.out.println(esportes);
    }
}
