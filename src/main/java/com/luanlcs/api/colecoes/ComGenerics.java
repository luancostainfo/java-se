package com.luanlcs.api.colecoes;

import java.util.ArrayList;

public class ComGenerics<E> {
    private E elemento;

    public E getElemento() {
        return elemento;
    }

    public void setElemento(E elemento) {
        this.elemento = elemento;
    }

    public double soma(ArrayList<? extends Number> lista) {
        return lista.stream().mapToDouble(Number::doubleValue).sum();
    }

    public static void main(String[] args) {

        ComGenerics<String> comGenerics = new ComGenerics<>();
        comGenerics.setElemento("Lago Paranoá");
        String texto = comGenerics.getElemento();
        System.out.println(texto);

        ArrayList<Double> doubles = new ArrayList<>();
        doubles.add(1.0);
        doubles.add(1.0);
        doubles.add(1.0);

        ArrayList<Float> floats = new ArrayList<>();
        floats.add(1.0F);
        floats.add(1.0F);
        floats.add(1.0F);

        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(1);
        integers.add(1);

        System.out.println(comGenerics.soma(doubles));
        System.out.println(comGenerics.soma(floats));
        System.out.println(comGenerics.soma(integers));
    }
}
