package com.luanlcs.api.colecoes;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ColecaoMap {
    public static void main(String[] args) {

        Map<String, String> paises = new HashMap<>();
        paises.put("BR", "Brasil");
        paises.put("RU", "Rússia");
        paises.put("IN", "India");
        paises.put("CH", "China");

        System.out.println(paises);

        System.out.println(paises.containsKey("BR"));
        System.out.println(paises.containsKey("AS"));

        System.out.println(paises.containsValue("Brasil"));
        System.out.println(paises.containsValue("África do Sul"));

        String brasil = paises.get("BR");
        System.out.println(brasil);

        String paiseRemovido = paises.remove("RU");
        System.out.println(paiseRemovido);

        System.out.println(paises);

        Set<String> chaves = paises.keySet();
        System.out.println(chaves);

        Collection<String> valores = paises.values();
        System.out.println(valores);

        for (String chave : chaves) {
            System.out.println(chave + " - " + paises.get(chave));
        }
    }
}
