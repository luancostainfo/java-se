package com.luanlcs.api.colecoes;

public class SemGenerics {

    private Object elemento;

    public Object getElemento() {
        return elemento;
    }

    public void setElemento(Object elemento) {
        this.elemento = elemento;
    }

    public static void main(String[] args) {

        SemGenerics semGenerics = new SemGenerics();
        semGenerics.setElemento("Lago Paranoá");
//        semGenerics.setElemento(11111); // Para simular ClassCastException

        String texto = (String) semGenerics.getElemento();
        System.out.println(texto.toUpperCase());
    }
}
