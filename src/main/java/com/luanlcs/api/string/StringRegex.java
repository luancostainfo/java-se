package com.luanlcs.api.string;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringRegex {
    public static void main(String[] args) {

        boolean b = "Java".matches("Java");
        System.out.println(b);

        /*
         * Modificadores
         * (?i) - Ignora maiúsculas e minúsculas
         * (?x) - Comentários
         * (?m) - Multilinhas
         * (?s) - Dottal
         */

        b = "Java".matches("(?i)java");
        System.out.println(b);

        b = "Java".matches("(?im)java");
        System.out.println(b);

        /*
         * Metacaracteres
         * . - Qualquer caracter
         * \d - dígitos [0-9]
         * \D - Não é dígito [^0-9]
         * \s - espaços [\t\n\x0b\f\r]
         * \S - Não é espaço [^\s]
         * \w - letra [a-zA-Z_0-9]
         * \W - Não é letra
         */
        b = "@".matches(".");
        System.out.println(b);

        b = "1".matches("\\d");
        System.out.println(b);

        b = "a".matches("\\w");
        System.out.println(b);

        b = " ".matches("\\s");
        System.out.println(b);

        b = "Pi".matches("..");
        System.out.println(b);

        b = "Pie".matches("...");
        System.out.println(b);

        b = "21".matches("\\d\\d");
        System.out.println(b);

        b = "72345-205".matches("\\d\\d\\d\\d\\d-\\d\\d\\d");
        System.out.println(b);

        /*
         * Quantificadores
         * X{n} - X, exatamente n vezes
         * X{n,} - X, pleo menos n vezes
         * X{n,m} - X, pelo menos n mas não mais que m vezes
         * X? - X, 0 ou 1 vez
         * X+ - X, 1 ou mais vezes
         * X* - X, 0 ou mais vezes
         */
        b = "21".matches("\\d{2}");
        System.out.println(b);

        b = "123".matches("\\d{2,}");
        System.out.println(b);

        b = "12345".matches("\\d{2,5}");
        System.out.println(b);

        b = "".matches(".?");
        System.out.println(b);

        b = "".matches(".*");
        System.out.println(b);

        b = "".matches(".+");
        System.out.println(b);

        b = "72345-205".matches("\\d{5}-\\d{3}");
        System.out.println(b);

        b = "17/11/1995".matches("\\d{2}/\\d{2}/\\d{4}");
        System.out.println(b);

        /*
         * Metacaracteres de fronteira
         * ^ - Inicia
         * $ - Finaliza
         * | - Ou
         */

        b = "Pier21".matches("^Pier.*");
        System.out.println(b);

        b = "Pier21".matches(".*21$");
        System.out.println(b);

        b = "tem java aqui".matches(".*java.*");
        System.out.println(b);

        b = "tem java aqui".matches("^tem.*aqui$");
        System.out.println(b);

        b = "sim".matches("sim|nao");
        System.out.println(b);

        b = "não".matches("sim|não");
        System.out.println(b);

        /*
         * Agrupadores
         * [...] - Agrupamento
         * [a-z] - Alcance
         * [a-e][i-i] - União
         * [a-z&&[aeiou]] - Interseção
         * [^abc] - Exceção
         * [a-z&&[^m-p]] - Subtração
         * \x - Fuga literal
         */

        b = "a".matches("[a-z]");
        System.out.println(b);

        b = "2".matches("[0-5]");
        System.out.println(b);

        b = "true".matches("[Tt]rue");
        System.out.println(b);

        b = "Yes".matches("[Tt]rue|[Yy]es");
        System.out.println(b);

        b = "Luan".matches("[A-Z][a-z]+");
        System.out.println(b);

        b = "Olho".matches("[^abc]lho");
        System.out.println(b);

        b = "crau".matches("cr[ae]u");
        System.out.println(b);

        b = "creu".matches("cr[ae]u");
        System.out.println(b);

        b = "rh@xti.com".matches("\\w+@\\w+\\.\\w{2,3}");
        System.out.println(b);

        // Pattern.compile

        String doce = "Qual é o Doce mais doCe que o doce?";

        Matcher matcher = Pattern.compile("(?i)doce").matcher(doce);
        while (matcher.find()) {
            System.out.println(matcher.group()); // recupera a ocorrencia
        }

        // Substituições
        System.out.println(doce);

        String replaceAll = doce.replaceAll("(?i)doce", "DOCINHO");
        System.out.println(replaceAll);

        String rato = "O rato roeu a roupa do rei de roma";
        String replaceAll2 = rato.replaceAll("r[aeiou]", "XX");
        System.out.println(rato);
        System.out.println(replaceAll2);

        String textoTabulado = "Tabular este texto".replaceAll("\\s", "\t");
        System.out.println(textoTabulado);

        // Variáveis
        String url = "www.xti.com.br/clientes-2011.html";
        // novo padrao - http://www.xti.com.br/2011/clientes.jsp
        String padrao = "(www.xti.com.br)/(\\w{2,})-(\\d{4}).html";

        System.out.println(url);
        url = url.replaceAll(padrao, "http://$1/$3/$2.jsp");
        System.out.println(url);
    }
}
