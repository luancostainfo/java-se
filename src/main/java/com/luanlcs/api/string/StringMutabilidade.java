package com.luanlcs.api.string;

public class StringMutabilidade {
    public static void main(String[] args) {

        StringBuilder frase = new StringBuilder("Java é");

        System.out.println(frase.length());
        System.out.println(frase.capacity());

        frase.append(" uma linguagem de programação");
        System.out.println(frase);

        frase.append(" muito legal.");
        System.out.println(frase);

        String url = new StringBuilder("www.xti.com.br").delete(0, 4).toString();
        System.out.println(url);
    }
}
