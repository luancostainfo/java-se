package com.luanlcs.api.string;

@SuppressWarnings("all")
public class SpringOperacoes {
    public static void main(String[] args) {

        // Construção
        String s1 = "Luan"; // literal
        String s2 = "Luan" + " da Costa"; // concatenacao
        String s3 = new String("Luan da Costa"); // construtor

        char[] letras = {'L', 'u', 'a', 'n'};
        String s4 = new String(letras);

        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);
        System.out.println(s4);

        // Operações/Localização
        String s5 = "Curso de Java Completo";

        int tamanho = s5.length();
        System.out.println(tamanho);

        char primeiroCaractere = s5.charAt(0);
        System.out.println(primeiroCaractere);

        int posicaoDeJava = s5.indexOf("Java");
        System.out.println(posicaoDeJava);

        boolean estaVazia = s5.isEmpty();
        System.out.println(estaVazia);
        System.out.println("   ".isEmpty());
        System.out.println("   ".isBlank());

        // Comparação
        String s6 = "Brasil";

        System.out.println(s6.equals("Brasil"));
        System.out.println(s6.equals("brasil"));

        System.out.println(s6.equalsIgnoreCase("Brasil"));
        System.out.println(s6.equalsIgnoreCase("brasil"));

        System.out.println(s6.toLowerCase().contains("br"));

        System.out.println(s6.startsWith("Br"));
        System.out.println(s6.endsWith("il"));

        // Extração

        String frase = "Luan da Costa Silva";

        String substring1 = frase.substring(5);
        System.out.println(substring1);

        String substring2 = frase.substring(0, 4);
        System.out.println(substring2);

        // 'Modificacao'
        String frase1 = "Java é legal";
        System.out.println(frase1);

        frase1 = frase1.concat(" e divertido.");
        System.out.println(frase1);

        frase1 = frase1.replace("Java", "C#");
        System.out.println(frase1);

        frase1 = frase1.replaceFirst("\\s", "X");
        System.out.println(frase1);

        frase1 = frase1.replaceAll("\\s|X", "--");
        System.out.println(frase1);

        frase1 = frase1.replaceAll("--", " ");
        System.out.println(frase1);

        frase1 = frase1.toUpperCase();
        System.out.println(frase1);

        frase1 = frase1.toLowerCase();
        System.out.println(frase1);


        String suja = "       Varios espaços antes e depois        ";
        System.out.println(suja.length());

        suja = suja.trim();
        System.out.println(suja.length());
    }
}
