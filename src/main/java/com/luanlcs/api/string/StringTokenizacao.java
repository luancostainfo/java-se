package com.luanlcs.api.string;

public class StringTokenizacao {
    public static void main(String[] args) {

        String cursos = "XHTML;CSS;JAVASCRIPT;JQUERY;JAVA";
        String[] tokens = cursos.split(";");
        for (String token : tokens) {
            System.out.println(token);
        }

    }
}
