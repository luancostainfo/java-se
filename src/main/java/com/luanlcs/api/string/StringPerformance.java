package com.luanlcs.api.string;

@SuppressWarnings("all")
public class StringPerformance {

    public static final int QUANTIDADE_ITERACOES = 1000_000;

    public static void main(String[] args) {

        long tempoInicial = System.currentTimeMillis();

//        concatenarComStringImutavel();
        concatenarComStringMutavel();

        long tempoFinal = System.currentTimeMillis();
        double total = (tempoFinal - tempoInicial) / 1000.0;
        System.out.println(total);
    }

    private static void concatenarComStringMutavel() {
        StringBuilder stringMutavel = new StringBuilder();
        for (int i = 0; i < QUANTIDADE_ITERACOES; i++) {
            stringMutavel.append(" - ").append(i);
        }
        System.out.println(stringMutavel);
    }

    private static void concatenarComStringImutavel() {
        String stringImutavel = new String();
        for (int i = 0; i < QUANTIDADE_ITERACOES; i++) {
            stringImutavel = stringImutavel + " - " + i;
        }
        System.out.println(stringImutavel);
    }
}
