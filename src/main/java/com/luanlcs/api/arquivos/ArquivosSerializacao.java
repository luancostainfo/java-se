package com.luanlcs.api.arquivos;

import com.luanlcs.oo.fundamentos.Funcionario;

import java.io.*;
import java.util.List;

@SuppressWarnings("all")
public class ArquivosSerializacao {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        List<Funcionario> funcionarios = List.of(
                new Funcionario("Luan", "Dev Java", 9200, true),
                new Funcionario("Rafael", "Dev Objective C", 10800, true)
        );

        FileOutputStream fileOutputStream = new FileOutputStream("files/funcionarios.ser");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(funcionarios);

        FileInputStream fileInputStream = new FileInputStream("files/funcionarios.ser");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        List<Funcionario> funcionariosList = (List<Funcionario>) objectInputStream.readObject();
        System.out.println(funcionariosList);
    }
}
