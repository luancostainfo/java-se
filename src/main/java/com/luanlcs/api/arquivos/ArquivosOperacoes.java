package com.luanlcs.api.arquivos;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class ArquivosOperacoes {
    public static void main(String[] args) throws IOException {

        Path path = Paths.get("files/arquivo1.txt");

        System.out.println(Files.exists(path));
        System.out.println(Files.isDirectory(path));
        System.out.println(Files.isRegularFile(path));
        System.out.println(Files.isReadable(path));
        System.out.println(Files.isExecutable(path));
        System.out.println(Files.size(path));
        System.out.println(Files.getLastModifiedTime(path));
        System.out.println(Files.getOwner(path));
        System.out.println(Files.probeContentType(path));

        // EXCLUSAO

        // Files.delete(path); // pode lançar exceções
        // Files.deleteIfExists(path); // nao lanca exceções

        // CRIAÇÃO
        Path path2 = Paths.get("files/arquivo5.txt");
        if (!Files.exists(path)) {
            Files.createFile(path2);
        }
        Files.write(path2, "Meu Texto".getBytes());

        // COPIA
        Path copia = Paths.get("files/arquivo-5-copia.txt");
        Files.copy(path2, copia, StandardCopyOption.REPLACE_EXISTING);

        // MOVE
        Path movido = Paths.get("files/move/arquivo-5-movido.txt");
        Files.createDirectories(movido.getParent());
        Files.move(copia, movido, StandardCopyOption.REPLACE_EXISTING);
    }
}
