package com.luanlcs.api.arquivos;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ArquivosLeituraJava7 {
    public static void main(String[] args) {

        Path path = Paths.get("files/arquivo3.txt");
        try (BufferedReader br = Files.newBufferedReader(path)) {
            String linha = br.readLine();
            while (linha != null) {
                System.out.println(linha);
                linha = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
