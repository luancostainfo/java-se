package com.luanlcs.api.arquivos;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Arquivos1 {
    public static void main(String[] args) throws IOException {

        Path path = Paths.get("files/arquivo1.txt");

        System.out.println(path.toAbsolutePath());
        System.out.println(path.getParent());
        System.out.println(path.getRoot());
        System.out.println(path.getFileName());

        // Criação de diretórios
        Files.createDirectories(path.getParent());

        // Escrever e ler arquivos

        // útil para arquivos pequenos - cria, limpa, escreve
        Files.write(path, "Meu Texto".getBytes());

        byte[] bytes = Files.readAllBytes(path);
        System.out.println(new String(bytes));
    }
}
