package com.luanlcs.api.arquivos;

import java.io.*;

@SuppressWarnings("all")
public class ArquivosLeituraJava6 {
    public static void main(String[] args) {

        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            // lê bytes
            fis = new FileInputStream("files/arquivo2.txt");

            // lê caracteres
            isr = new InputStreamReader(fis);

            // lê linhas
            br = new BufferedReader(isr);

            String linha = br.readLine();
            while (linha != null) {
                System.out.println(linha);
                linha = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
