package com.luanlcs.api.arquivos;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ArquivosEscritaJava7 {
    public static void main(String[] args) {

        Path path = Paths.get("files/arquivo3.txt");
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {
            bufferedWriter.write("Luan");
            bufferedWriter.newLine();

            bufferedWriter.write("Rafael");
            bufferedWriter.newLine();

            bufferedWriter.write("Lourival");
            bufferedWriter.newLine();

            System.out.println("Arquivo escrito com Java 7");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
