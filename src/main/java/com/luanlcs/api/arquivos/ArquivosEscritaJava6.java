package com.luanlcs.api.arquivos;

import java.io.*;

@SuppressWarnings("all")
public class ArquivosEscritaJava6 {
    public static void main(String[] args) {
        // escrita antes do java 7
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        BufferedWriter bw = null;

        try {
            // escreve bytes
            fos = new FileOutputStream("files/arquivo2.txt");

            // escreve caracteres
            osw = new OutputStreamWriter(fos);

            // escreve linhas
            bw = new BufferedWriter(osw);

            bw.write("Luan");
            bw.newLine();

            bw.write("Alen");
            bw.newLine();

            bw.write("Igor");
            bw.newLine();

            bw.write("Gabriel");
            bw.newLine();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bw != null) {

                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Arquivo escrito com sucesso usando Java 7");
        }

    }
}
