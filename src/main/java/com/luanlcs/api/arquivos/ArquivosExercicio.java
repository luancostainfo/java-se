package com.luanlcs.api.arquivos;

import com.luanlcs.oo.fundamentos.Funcionario;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ArquivosExercicio {

    private static final Path path = Paths.get("files/funcionarios.txt");

    public static void main(String[] args) {

        List<Funcionario> funcionarios = List.of(
                new Funcionario("Luan", "Dev Java", 9200, true),
                new Funcionario("Rafael", "Dev C", 10800, true),
                new Funcionario("Lourival", "Dev Angular", 12500, true),
                new Funcionario("Pedro", "UX Designer", 13000, true)
        );
        armazenarFuncionariosEmArquivo(funcionarios);

        List<Funcionario> funcionarios2 = lerFuncionariosDeArquivo();
        funcionarios2.forEach(System.out::println);
    }

    private static List<Funcionario> lerFuncionariosDeArquivo() {
        ArrayList<Funcionario> funcionarios = new ArrayList<>();
        try (BufferedReader br = Files.newBufferedReader(ArquivosExercicio.path)) {
            String linha = br.readLine();
            while (linha != null) {
                String[] tokens = linha.split(";");
                String nome = tokens[0];
                String profissao = tokens[1];
                double salario = Double.parseDouble(tokens[2]);
                boolean ativo = Boolean.parseBoolean(tokens[3]);

                Funcionario funcionario = new Funcionario(nome, profissao, salario, ativo);
                funcionarios.add(funcionario);

                linha = br.readLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return funcionarios;
    }

    private static void armazenarFuncionariosEmArquivo(List<Funcionario> funcionarios) {
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(ArquivosExercicio.path)) {

            for (Funcionario funcionario : funcionarios) {
                String nome = funcionario.getNome();
                String profissao = funcionario.getProfissao();
                double salario = funcionario.getSalario();
                boolean ativo = funcionario.isAtivo();

                String linha = String.format("%s;%s;%s;%s", nome, profissao, salario, ativo);
                bufferedWriter.write(linha);
                bufferedWriter.newLine();
            }

            System.out.println("Funcionarios gravados com sucesso!");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
