package com.luanlcs.api.data;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DatasInternacionalizacao {
    public static void main(String[] args) {

        // O Locale utilizado pelos formatadores varia de acordo com o idioma do sistema operacional.
        Locale localeDefault = Locale.getDefault();
        System.out.println(localeDefault);

        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();

        DateFormat formatador = DateFormat.getDateInstance(DateFormat.FULL);
        System.out.println(formatador.format(date));

        formatador = DateFormat.getDateInstance(DateFormat.FULL, new Locale("pt", "BR"));
        System.out.println(formatador.format(date));

        formatador = DateFormat.getDateInstance(DateFormat.FULL, new Locale("hi", "IN"));
        System.out.println(formatador.format(date));

        formatador = DateFormat.getDateInstance(DateFormat.FULL, Locale.US);
        System.out.println(formatador.format(date));

        formatador = DateFormat.getDateInstance(DateFormat.FULL, Locale.FRANCE);
        System.out.println(formatador.format(date));

        formatador = DateFormat.getDateInstance(DateFormat.FULL, Locale.JAPAN);
        System.out.println(formatador.format(date));

        formatador = DateFormat.getDateInstance(DateFormat.FULL, Locale.ITALY);
        System.out.println(formatador.format(date));

    }
}
