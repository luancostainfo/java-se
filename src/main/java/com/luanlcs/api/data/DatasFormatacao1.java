package com.luanlcs.api.data;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DatasFormatacao1 {
    public static void main(String[] args) {

        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();

        DateFormat formatador = DateFormat.getDateInstance();
        String formatada = formatador.format(date);
        System.out.println(formatada);

        formatador = DateFormat.getTimeInstance();
        formatada = formatador.format(date);
        System.out.println(formatada);

        formatador = DateFormat.getDateTimeInstance();
        formatada = formatador.format(date);
        System.out.println(formatada);

        System.out.println("--------------");

        // Aplicando estilos

        formatador = DateFormat.getDateInstance(DateFormat.FULL);
        formatada = formatador.format(date);
        System.out.println(formatada);

        formatador = DateFormat.getDateInstance(DateFormat.LONG);
        formatada = formatador.format(date);
        System.out.println(formatada);

        formatador = DateFormat.getDateInstance(DateFormat.MEDIUM);
        formatada = formatador.format(date);
        System.out.println(formatada);

        formatador = DateFormat.getDateInstance(DateFormat.SHORT);
        formatada = formatador.format(date);
        System.out.println(formatada);

        System.out.println("-------------");

        // Convertendo uma String para Date

        formatador = DateFormat.getDateInstance(DateFormat.SHORT, new Locale("pt", "BR"));
        String data = "17/11/1995";
        try {
            Date dateConvertida = formatador.parse(data);
            System.out.println(dateConvertida);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
    }
}
