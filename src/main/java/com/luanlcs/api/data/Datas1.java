package com.luanlcs.api.data;

import java.util.Date;

public class Datas1 {
    public static void main(String[] args) {

        // 01/01/1970
        System.out.println(System.currentTimeMillis());

        Date agora = new Date();
        System.out.println(agora);

        // Informando a quantidade de milisegundos
        Date date = new Date(1_000_000_000_000L);
        System.out.println(date);

        System.out.println(date.getTime()); // quantidade de milisegundos

        date.setTime(9_000_000_000_000L); // setando a quantidade de milisegundos
        System.out.println(date.getTime());
        System.out.println(date);

        System.out.println(agora.compareTo(date)); // comparando duas datas
    }
}
