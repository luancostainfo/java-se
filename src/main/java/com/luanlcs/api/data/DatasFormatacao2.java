package com.luanlcs.api.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DatasFormatacao2 {
    public static void main(String[] args) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String string = simpleDateFormat.format(new Date());
        System.out.println(string);

        String data = "17/11/1995";
        try {
            Date parse = simpleDateFormat.parse(data);
            System.out.println(parse);
        } catch (ParseException e) {
            System.out.println("Erro ao realizar a conversão");
        }

    }
}
