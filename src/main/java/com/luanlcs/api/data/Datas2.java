package com.luanlcs.api.data;

import java.util.Calendar;
import java.util.Date;

public class Datas2 {
    public static void main(String[] args) {

        long currentTimeMillis = System.currentTimeMillis();
        System.out.println(currentTimeMillis);

        // definindo data
        Calendar calendar = Calendar.getInstance();
        calendar.set(1995, Calendar.NOVEMBER, 17, 18, 30, 30);

        System.out.println(calendar.get(Calendar.YEAR));
        System.out.println(calendar.get(Calendar.MONTH));
        System.out.println(calendar.get(Calendar.DAY_OF_MONTH));
        System.out.println(calendar.get(Calendar.HOUR_OF_DAY));
        System.out.println(calendar.get(Calendar.MINUTE));
        System.out.println(calendar.get(Calendar.SECOND));

        Date date = calendar.getTime();
        System.out.println(date);

        // Data Atual
        Calendar calendar2 = Calendar.getInstance();
        System.out.println(calendar2.getTime());

        // alterando data
        calendar2.set(Calendar.YEAR, 2025);
        calendar2.set(Calendar.MONTH, Calendar.JANUARY);
        calendar2.set(Calendar.DAY_OF_MONTH, 1);

        System.out.println(calendar2.getTime());

        // limpando campos da data
        calendar2.clear(Calendar.HOUR);
        calendar2.clear(Calendar.MINUTE);
        calendar2.clear(Calendar.SECOND);
        System.out.println(calendar2.getTime());

        // operacoes com datas

        Calendar hoje = Calendar.getInstance();
        System.out.println(hoje.getTime());

        hoje.add(Calendar.DAY_OF_MONTH, 5);
        System.out.println(hoje.getTime());

        hoje.add(Calendar.MONTH, 6);
        System.out.println(hoje.getTime());

        // o roll volta ao inicio se o mes estourar
        hoje.roll(Calendar.DAY_OF_MONTH, 25);
        System.out.println(hoje.getTime());

        // diminuindo
        hoje = Calendar.getInstance();
        hoje.add(Calendar.HOUR_OF_DAY, -1);
        System.out.println(hoje.getTime());

        // Saudação

        int hora = hoje.get(Calendar.HOUR_OF_DAY);
        if (hora <= 12) {
            System.out.println("Bom dia");
        } else if (hora < 18) {
            System.out.println("Boa tarde");
        } else {
            System.out.println("Boa noite");
        }
    }
}
