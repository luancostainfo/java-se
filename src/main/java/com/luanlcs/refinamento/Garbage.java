package com.luanlcs.refinamento;

import java.util.ArrayList;

@SuppressWarnings("all")
public class Garbage {

    private static final int MB = 1_048_576;

    public static long carregarMemoria() {
        ArrayList<Integer> numeros = new ArrayList<>();
        for (int i = 0; i < 100_000; i++) {
            numeros.add(i);
        }
        return Runtime.getRuntime().freeMemory() / MB;
    }

    public static void main(String[] args) {

        Runtime runtime = Runtime.getRuntime();
        long memoriaTotal = runtime.maxMemory() / MB;

        System.out.println("Memória disponível: " + memoriaTotal + "MB");

        long memoriaDisponivelAposCarregamento = carregarMemoria();

        double inicio = memoriaTotal - memoriaDisponivelAposCarregamento;
        System.out.println("Memória disponível após carregamento: " + inicio + "MB");

        runtime.runFinalization();
        runtime.gc();

        double fim = memoriaTotal - ((double) runtime.freeMemory() / MB);
        System.out.println("Após limpar a memória: " + fim + "MB");
    }
}
