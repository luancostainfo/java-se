package com.luanlcs.refinamento;

/*
 * Instituição:              Universidade XTI
 * Projeto:                  Sistema de Avaliações
 * Data de Criação:          08/10/2011
 * Criador:                  Luan da Costa
 * Revisão:                  2
 * */

@Cabecalho(
        instituicao = "Universidade XTI",
        projeto = "Sistema de Avaliações",
        dataCriacao = "08/10/2011",
        criador = "Luan da Costa",
        revisao = 2
)
@ErrosCorrigidos(
        {"0001", "0002"}
)
public class Anotacao {


    @Deprecated
    public void anotar() {
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
