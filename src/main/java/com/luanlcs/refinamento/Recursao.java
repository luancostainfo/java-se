package com.luanlcs.refinamento;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Recursao {

    private static int somarAte(int numeroMaximo) {
        // parte do problema que sabemos resolver - Caso onde o número é igual a 0
        if (numeroMaximo == 0) {
            return numeroMaximo;
        } else {
            System.out.printf("%d + ", numeroMaximo);
            return numeroMaximo + somarAte(numeroMaximo - 1);
        }
    }

    private static int potencia(int x, int y) {
        if (y == 1) {
            return x;
        } else {
            System.out.println(y);
            return x * potencia(x, y - 1);
        }
    }

    public static void listar(Path path) {
        if (Files.isRegularFile(path)) {
            System.out.println(path.toAbsolutePath());
        } else {
            String s = "\n" + path.toAbsolutePath();
            System.out.println("-------------" + s.toUpperCase() + "-------------");
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
                for (Path diretorio : stream) {
                    listar(diretorio);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(" = " + somarAte(10));
        System.out.println(potencia(3, 4));
        String property = System.getProperty("user.dir");

        listar(Paths.get(property));
    }
}
