package com.luanlcs.refinamento;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("all")
public class Aninhamento extends JFrame {

    private final JButton botao;

    public Aninhamento() {
        super("Aninhamento");

        botao = new JButton("OK");
        botao.addActionListener(new ListenerAninhado());
        botao.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Classe anônima executa: " + botao.getText());
            }
        });

        getContentPane().add(botao);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 300);
        setVisible(true);
    }

    public class ListenerAninhado implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("Classe aninhada executa: " + botao.getText());
        }
    }


    public static void main(String[] args) {
        Aninhamento aninhamento = new Aninhamento();
    }
}
