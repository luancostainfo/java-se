package com.luanlcs.refinamento;

/**
 * Classe utilizada para demonstrar algumas <i>funcionalidades</i> do <b>Javadoc</b>
 *
 * @author Luan da Costa
 * @version 1.0
 * @see Math
 * @since 1.0
 */
@SuppressWarnings("all")
public class JavaDoc {

    private String atributo;

    /**
     * Método que retorna o atributo
     *
     * @return atributo
     */
    public String getAtributo() {
        return atributo;
    }

    /**
     * Método para definir um valor para o atributo
     *
     * @param atributo é uma string qualquer
     */
    public void setAtributo(String atributo) throws ClassCastException {
        this.atributo = atributo;
    }
}
