package com.luanlcs.oo.heranca;

public final class Galinha extends Animal {
    public Galinha(String comida, double peso) {
        super(comida, peso);
    }

    @Override
    public void fazerBarulho() {
        System.out.println("Có, có!");
    }

    public void ciscar() {
        System.out.println("Ciscando");
    }
}
