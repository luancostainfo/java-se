package com.luanlcs.oo.heranca;

public class Cachorro extends Animal {
    public Cachorro(String comida, double peso) {
        super(comida, peso);
    }

    @Override
    public void fazerBarulho() {
        System.out.println("Au, au!");
    }
}
