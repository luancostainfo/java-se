package com.luanlcs.oo.heranca;

@SuppressWarnings("all")
public class AnimalTest {
    public static void main(String[] args) {

        Cachorro cachorro = new Cachorro("Ração", 30);
        System.out.println(cachorro.getComida());
        System.out.println(cachorro.getPeso());
        cachorro.fazerBarulho();

        Galinha galinha = new Galinha("Milho", 10);
        System.out.println(galinha.getComida());
        System.out.println(galinha.getPeso());
        galinha.fazerBarulho();

        System.out.println(cachorro instanceof Cachorro);
        System.out.println(cachorro instanceof Animal);

        System.out.println(cachorro.toString());
        System.out.println(galinha.toString());

        Cachorro cachorro2 = new Cachorro("Ração", 30);
        System.out.println(cachorro.equals(cachorro2));
        System.out.println(cachorro.hashCode() == cachorro2.hashCode());
    }
}
