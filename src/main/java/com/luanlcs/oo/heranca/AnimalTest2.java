package com.luanlcs.oo.heranca;

public class AnimalTest2 {

    public static void fazerBarulhoComPolimorfismo(Animal animal) {
        animal.fazerBarulho();
    }

    public static void fazerBarulhoSemPolimorfismo(String animal) {
        if (animal.equals("Cachorro")) {
            System.out.println("Au, au!");
        } else if (animal.equals("Galinha")) {
            System.out.println("Có, có!");
        }
        // um else if para cada nova entidade que venha a entrar no sistema
    }

    public static void main(String[] args) {

        Cachorro cachorro = new Cachorro("Ração", 15);
        Animal cachorro2 = new Cachorro("Ração", 15);

        Galinha galinha = new Galinha("Milho", 20);
        Animal galinha2 = new Galinha("Milho", 20);

        fazerBarulhoComPolimorfismo(cachorro);
        fazerBarulhoComPolimorfismo(cachorro2);
        fazerBarulhoComPolimorfismo(galinha);
        fazerBarulhoComPolimorfismo(galinha2);

        fazerBarulhoSemPolimorfismo("Cachorro");
        fazerBarulhoSemPolimorfismo("Galinha");

        galinha.ciscar();

    }
}
