package com.luanlcs.oo.heranca;

import java.util.Objects;

public abstract class Animal {
    private final String comida;
    private final double peso;

    public Animal(String comida, double peso) {
        this.comida = comida;
        this.peso = peso;
    }

    public abstract void fazerBarulho();

    public String getComida() {
        return comida;
    }

    public double getPeso() {
        return peso;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "comida='" + comida + '\'' +
                ", peso=" + peso +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return Double.compare(animal.peso, peso) == 0 && Objects.equals(comida, animal.comida);
    }

    @Override
    public int hashCode() {
        return Objects.hash(comida, peso);
    }
}
