package com.luanlcs.oo.interfaces;

public class Quadrado implements AreaCalculavel{

    private final double lado;

    public Quadrado(double lado) {
        this.lado = lado;
    }

    @Override
    public double calculaArea() {
        return lado * lado;
    }
}
