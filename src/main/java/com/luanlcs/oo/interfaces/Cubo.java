package com.luanlcs.oo.interfaces;

public class Cubo implements AreaCalculavel, VolumeCalculavel {

    private final double lado;

    public Cubo(double lado) {
        this.lado = lado;
    }

    @Override
    public double calculaArea() {
        return 6 * lado * lado;
    }

    @Override
    public double calculaVolume() {
        return lado * lado * lado;
    }
}
