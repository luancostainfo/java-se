package com.luanlcs.oo.interfaces;

public interface VolumeCalculavel {
    double calculaVolume();
}
