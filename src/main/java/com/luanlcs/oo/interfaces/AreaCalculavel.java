package com.luanlcs.oo.interfaces;

public interface AreaCalculavel {
    double calculaArea();
}
