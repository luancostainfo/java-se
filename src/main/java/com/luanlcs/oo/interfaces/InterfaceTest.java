package com.luanlcs.oo.interfaces;

public class InterfaceTest {
    public static void imprimirArea(AreaCalculavel areaCalculavel) {
        System.out.println(areaCalculavel.calculaArea());
    }

    public static void imprimirVolume(VolumeCalculavel volumeCalculavel) {
        System.out.println(volumeCalculavel.calculaVolume());
    }

    public static void main(String[] args) {

        Quadrado quadrado = new Quadrado(2);
        Cubo cubo = new Cubo(5);

        imprimirArea(quadrado);

        imprimirArea(cubo);
        imprimirVolume(cubo);
    }
}
