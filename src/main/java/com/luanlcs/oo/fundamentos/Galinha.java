package com.luanlcs.oo.fundamentos;

public class Galinha {
    private int quantidadeDeOvos;
    private static int quantidadeDeOvosDaGranja;

    public Galinha botar() {
        this.quantidadeDeOvos++;
        Galinha.quantidadeDeOvosDaGranja++;
        return this;
    }

    public int getQuantidadeDeOvos() {
        return quantidadeDeOvos;
    }

    public static int getQuantidadeDeOvosDaGranja() {
        return quantidadeDeOvosDaGranja;
    }
}
