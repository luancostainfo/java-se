package com.luanlcs.oo.fundamentos;

public enum PecasXadrezEnum {
    PEAO,
    TORRE,
    BISPO,
    CAVALO,
    REI,
    RAINHA
}
