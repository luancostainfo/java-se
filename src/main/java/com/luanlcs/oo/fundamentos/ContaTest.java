package com.luanlcs.oo.fundamentos;

public class ContaTest {
    public static void main(String[] args) {

        Conta conta1 = new Conta();
        conta1.cliente = "Luan";
        conta1.saldo = 10_000;
        conta1.exibeSaldo();

        Conta conta2 = new Conta();
        conta2.cliente = "Alen";
        conta2.saldo = 20_000;
        conta2.exibeSaldo();

        conta1.deposita(5_000);
        conta1.exibeSaldo();

        conta1.saca(3_000);
        conta1.exibeSaldo();

        conta1.transferePara(conta2, 7_000);
        conta1.exibeSaldo();
        conta2.exibeSaldo();
    }
}
