package com.luanlcs.oo.fundamentos;

public class MatematicaTest {
    public static void main(String[] args) {

        Matematica matematica = new Matematica();
        int maior = matematica.maior(10, 20);
        System.out.println(maior);

        double soma = matematica.soma(50, 70);
        System.out.println(soma);

        int raiz = matematica.raiz(9);
        System.out.println(raiz);

        System.out.println(matematica.raiz(100));
        System.out.println(Math.sqrt(100));

        double somaVarArgs = matematica.soma(10, 20, 30);
        System.out.println(somaVarArgs);

        double media1 = matematica.media(10, 5, 7.5);
        System.out.println(media1);

        double media2 = matematica.media(10, 5);
        System.out.println(media2);

        double media3 = matematica.media("10", "5");
        System.out.println(media3);

    }
}
