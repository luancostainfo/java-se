package com.luanlcs.oo.fundamentos;

public class Carro {
    private String modelo;
    private int velocidadeMaxima;
    private double segundosZeroACem;
    private Motor motor;

    public Carro() {
    }

    public Carro(String modelo, int velocidadeMaxima, double segundosZeroACem) {
        this.modelo = modelo;
        this.velocidadeMaxima = velocidadeMaxima;
        this.segundosZeroACem = segundosZeroACem;
    }

    public Carro(String modelo, int velocidadeMaxima, double segundosZeroACem, Motor motor) {
        this(modelo, velocidadeMaxima, segundosZeroACem);
        this.motor = motor;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setVelocidadeMaxima(int velocidadeMaxima) {
        this.velocidadeMaxima = velocidadeMaxima;
    }

    public void setSegundosZeroACem(double segundosZeroACem) {
        this.segundosZeroACem = segundosZeroACem;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }

    public void imprimirInformacoes() {
        System.out.println("Modelo: " + this.modelo);
        System.out.println("Velocidade Máxima: " + this.velocidadeMaxima);
        System.out.println("Segundos de zero a cem: " + this.segundosZeroACem);
        if (motor != null) {
            motor.imprimirInformacoes();
        }
        System.out.println();
    }
}
