package com.luanlcs.oo.fundamentos;

import java.util.Arrays;

public class Matematica {

    public int maior(int x, int y) {
        return Math.max(x, y);
    }

    public double soma(double x, double y) {
        return x + y;
    }

    public int raiz(int numero) {
        int contador = 0;
        for (int i = 1; i <= numero; i += 2) {
            numero -= i;
            contador++;
        }
        return contador;
    }

    public double soma(double... valores) {
        return Arrays.stream(valores).sum();
    }

    public double media(double x, double y) {
        System.out.println("double media(double x, double y)");
        return this.soma(x, y) / 2;
    }

    public double media(String x, String y) {
        System.out.println("double media(String x, String y)");
        return this.soma(Double.parseDouble(x), Double.parseDouble(y)) / 2;
    }

    public double media(double... valores) {
        System.out.println("double media(double... valores)");
        return this.soma(valores) / valores.length;
    }
}
