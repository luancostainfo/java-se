package com.luanlcs.oo.fundamentos;

public class GalinhaTest {
    public static void main(String[] args) {

        Galinha galinha1 = new Galinha();
        galinha1.botar().botar().botar();

        Galinha galinha2 = new Galinha();
        galinha2.botar().botar();

        System.out.println("Quantidade de ovos da galinha1: " + galinha1.getQuantidadeDeOvos());
        System.out.println("Quantida de ovos da galinha2: " + galinha2.getQuantidadeDeOvos());
        System.out.println("Total de ovos da granja: " + Galinha.getQuantidadeDeOvosDaGranja());
    }
}
