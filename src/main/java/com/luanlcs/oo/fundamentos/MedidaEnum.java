package com.luanlcs.oo.fundamentos;

public enum MedidaEnum {
    MM("Milímetro"),
    CM("Centímetro"),
    M("Metro");

    private final String descricao;

    MedidaEnum(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

}
