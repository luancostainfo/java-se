package com.luanlcs.oo.fundamentos;

public class Motor {
    private String tipo;
    private int potencia;

    public Motor() {
    }

    public Motor(String tipo, int potencia) {
        this.tipo = tipo;
        this.potencia = potencia;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setPotencia(int potencia) {
        this.potencia = potencia;
    }

    public void imprimirInformacoes() {
        System.out.println("Tipo Motor: " + this.tipo);
        System.out.println("Potência: " + this.potencia);
    }
}
