package com.luanlcs.oo.fundamentos;

public class CarroTest {
    public static void main(String[] args) {

        Carro ferrari = new Carro();
        ferrari.setModelo("Ferrari Enzo");
        ferrari.setSegundosZeroACem(3.2);
        ferrari.setVelocidadeMaxima(349);
        ferrari.imprimirInformacoes();

        Carro koenigsegg = new Carro("Koenigsegg", 430, 3.1);
        koenigsegg.imprimirInformacoes();

    }
}
