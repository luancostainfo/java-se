package com.luanlcs.oo.fundamentos;

import java.util.Arrays;

public class EnumTest {

    public static final double PI = 3.14; // constante local da classe

    public static void main(String[] args) {

        System.out.println(PI);

        System.out.println(PecasXadrezEnum.BISPO);
        System.out.println(PecasXadrezEnum.REI);

        System.out.println(MedidaEnum.M);
        System.out.println(MedidaEnum.M.getDescricao());

        MedidaEnum[] medidas = MedidaEnum.values();
        for (MedidaEnum medida : medidas) {
            System.out.println(medida + " - " + medida.getDescricao());
        }

        PecasXadrezEnum[] pecas = PecasXadrezEnum.values();
        Arrays.stream(pecas).forEach(System.out::println);
    }
}
