package com.luanlcs.oo.fundamentos;

public class FuncionarioTest {
    public static void main(String[] args) {
        Funcionario funcionario = new Funcionario();
        funcionario.setNome("Luan");
        funcionario.setProfissao("Programador");
        funcionario.setAtivo(true);
        funcionario.setSalario(9200);

        System.out.println(funcionario.getNome());
        System.out.println(funcionario.getProfissao());
        System.out.println(funcionario.isAtivo());
        System.out.println(funcionario.getSalario());
    }
}
