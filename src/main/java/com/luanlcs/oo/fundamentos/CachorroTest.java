package com.luanlcs.oo.fundamentos;

public class CachorroTest {
    public static void main(String[] args) {

        Cachorro pitbull = new Cachorro();
        pitbull.raca = "Pitbull";
        pitbull.tamanho = 40;
        pitbull.latir();
        System.out.println("Raça: " + pitbull.raca);
        System.out.println("Tamanho: " + pitbull.tamanho);

        System.out.println("------------------------------");

        Cachorro toto = new Cachorro();
        toto.raca = "Vira-lata";
        toto.tamanho = 10;
        toto.latir();
        System.out.println("Raça: " + toto.raca);
        System.out.println("Tamanho: " + toto.tamanho);
    }
}
