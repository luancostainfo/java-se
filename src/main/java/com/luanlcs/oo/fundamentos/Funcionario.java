package com.luanlcs.oo.fundamentos;

import java.io.Serializable;

public class Funcionario implements Serializable {
    private String nome;
    private String profissao;
    private double salario;
    private boolean ativo;

    public Funcionario(String nome, String profissao, double salario, boolean ativo) {
        this.nome = nome;
        this.profissao = profissao;
        this.salario = salario;
        this.ativo = ativo;
    }

    public Funcionario() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    @Override
    public String toString() {
        return "Funcionario{" +
                "nome='" + nome + '\'' +
                ", profissao='" + profissao + '\'' +
                ", salario=" + salario +
                ", ativo=" + ativo +
                '}';
    }
}
