package com.luanlcs.oo.fundamentos;

@SuppressWarnings("all")
public class Conta {
    String cliente;
    double saldo;

    void deposita(double valor) {
        this.saldo += valor;
    }

    void exibeSaldo() {
        System.out.println(this.cliente + " seu saldo é " + this.saldo);
    }

    void saca(double valor) {
        this.saldo -= valor;
    }

    void transferePara(Conta destino, double valor) {
        this.saca(valor);
        destino.deposita(valor);
    }
}
