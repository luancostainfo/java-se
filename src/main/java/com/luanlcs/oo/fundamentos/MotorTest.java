package com.luanlcs.oo.fundamentos;

public class MotorTest {
    public static void main(String[] args) {

        Motor v12 = new Motor();
        v12.setTipo("V12");
        v12.setPotencia(660);

        Carro ferrari = new Carro();
        ferrari.setModelo("Ferrari Enzo");
        ferrari.setSegundosZeroACem(3.2);
        ferrari.setVelocidadeMaxima(349);
        ferrari.setMotor(v12);
        ferrari.imprimirInformacoes();

        Carro koenigsegg = new Carro("Koenigsegg", 430, 3.1, new Motor("V8", 1018));
        koenigsegg.imprimirInformacoes();

    }
}
